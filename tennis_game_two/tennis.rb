require_relative 'player.rb'

class TennisGame2
  
  def initialize(player1Name, player2Name)
    @player1 = Player.new(player1Name)
    @player2 = Player.new(player2Name)
  end

  def won_point(playerName)
    if @player1.name == playerName
      @player1.add_point
    else
      @player2.add_point
    end
  end

  def score
    result = ""
    scoreboard = {
      0 => "Love",
      1 => "Fifteen",
      2 => "Thirty",
      3 => "Forty"
    }
    if (@player1.points == @player2.points and @player1.points < 3)
      result = scoreboard[@player1.points]
      result += "-All"
      return result
    end
    if (@player1.points==@player2.points and @player1.points>2)
      result = "Deuce"
      return result
    end
    
    p1res = scoreboard.fetch(@player1.points, '')
    p2res = scoreboard.fetch(@player2.points, '')
    result = p1res + "-" + p2res
    if (@player1.points > @player2.points and @player2.points >= 3)
      result = "Advantage " + @player1.name
    end
    if (@player2.points > @player1.points and @player1.points >= 3)
      result = "Advantage " + @player2.name
    end
    if (@player1.points>=4 and @player2.points>=0 and (@player1.points-@player2.points)>=2)
      result = "Win for " + @player1.name
    end
    if (@player2.points>=4 and @player1.points>=0 and (@player2.points-@player1.points)>=2)
      result = "Win for " + @player2.name
    end
    result
  end

  def setp1Score(number)
    (0..number).each do |i|
      player1.add_point
    end
  end

  def setp2Score(number)
    (0..number).each do |i|
      player2.add_point
    end
  end
end
